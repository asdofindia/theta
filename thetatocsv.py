#!/usr/bin/env python3

"""
This program reads a json and generates the appropriate csv file for the same
Usage:
  ./thetatocsv.py [--json <name>] [--form <string>]

Options:
  --json <name>     Name of the json file you want to convert [default: existingforms.json]
  --form <string>   A string to search for in the form name to decide whether to process the form or not
"""
import csv
import json
import logging
import os
import pprint
import sys

from docopt import docopt

try:
    loglevel = os.environ['LOGLEVEL']
except KeyError:
    loglevel = "warning"
numeric_level = getattr(logging, loglevel.upper(), None)
if not isinstance(numeric_level, int):
    raise ValueError('Invalid log level: %s' % loglevel)
logging.basicConfig(level=numeric_level)


class JtoC:
    """This class has the chunk of the function"""

    def __init__(self, arguments):
        """The init function. Duh."""
        jsonpath = arguments['--json']
        with open(jsonpath, 'r') as jsonfile:
            self.forms = json.load(jsonfile)
        csvfile = open(jsonpath[:-4]+'csv', 'w')
        self.writer = csv.writer(csvfile).writerow
        self.printer = pprint.PrettyPrinter(indent=2).pprint
        self.writeheader()
        if hasattr(arguments, "--form"):
            logging.debug("Set %s as form_filter" % arguments['--form'])
            self.form_filter = arguments['--form']

    def writeheader(self):
        """Writes the header row of the csv file"""
        self.writer("field_type,key,data_name,extras,label,kannada,malayalam,hindi,tamil".split(','))

    def run(self):
        """Does the conversion bit"""
        for index, form in enumerate(self.forms['forms']):
            logging.debug("Processing form %s" % index)
            if self.filter_form(form):
                self.process_form(form)
            else:
                logging.debug("Rejected through filter criteria")
    
    def filter_form(self, form):
        """
        Checks if the form filter mentioned in the command line is there in the name of the form
        """
        if not hasattr(self, "form_filter"):
            return True
        else:
            logging.debug("filter criteria is %s" % self.form_filter)
            return bool(self.form_filter in form['name'])

    def process_form(self, form):
        """Processes form and runs elements"""
        self.indent_level = -1
        self.process_elements(form['elements'])


    def process_elements(self, elements):
        """Processes each element by element"""
        self.indent_level += 1
        logging.debug("Processing elements at indent_level %s" % self.indent_level)
        for element in elements:
            self.process_element(element)
        self.indent_level -= 1

    def process_element(self, element):
        """Processes one element"""
        rowitems = []
        rowitems.append(
            self.indent(
                self.get_field_type(element)
                )
            )
        rowitems.append(element['key'])
        rowitems.append(element['data_name'])
        rowitems.append(self.get_extras(element))
        rowitems.append(element['label'])
        rowitems.append('')
        rowitems.append('')
        self.writer(rowitems)
        logging.debug("Wrote row %s", rowitems)
        if 'elements' in element:
            self.process_elements(element['elements'])
        if 'choices' in element:
            self.process_choices(element['choices'])

    def process_choices(self, choices):
        self.indent_level += 1
        for choice in choices:
            rowitems = []
            rowitems.append(self.indent("Choice"))
            rowitems.extend([""] * 3)
            rowitems.append(choice['label'])
            rowitems.extend([''] * 2)
            self.writer(rowitems)
        self.indent_level -= 1
    
    def indent(self, text):
        """
        Adds indent level to the field_type
        """
        indented_text = "-" * self.indent_level
        indented_text += text
        return indented_text

    @staticmethod
    def get_field_type(element):
        """
        It would have been easier to reuse the same field names from the server
        """
        field_type_server = ["TextField", "TextField", "ChoiceField", "Section", "ClassificationField", "Repeatable", "RecordLinkField"]
        field_type = ["Text", "Number", "SCQ", "Section", "Classification", "Repeatable", "RecordLink"]
        field_dict = dict(zip(field_type_server, field_type))
        return field_dict.get(element["type"], element["type"])

    def get_extras(self, element):
        extras = ''
        if element['required']:
            extras += 'required,'
        if 'default_previous_value' in element and element['default_previous_value']:
            extras += 'previous,'
        if 'allow_other' in element and element['allow_other']:
            extras += 'other,'
        if 'classification_set_id' in element:
            extras += 'set=%s,' % element['classification_set_id']
        if 'choice_list_id' in element:
            extras += 'set=%s,' % element['choice_list_id']
        if 'min_length' in element and element['min_length'] is not None:
            extras += 'minlength=%s,' % str(element['min_length'])
        if 'max_length' in element and element['max_length'] is not None:
            extras += 'maxlength=%s,' % str(element['max_length'])
        if 'min' in element and element['min'] is not None:
            extras += 'min=%s,' % str(element['min'])
        if 'max' in element and element['max'] is not None:
            extras += 'max=%s,' % str(element['max'])
        if 'visible_conditions' in element and element['visible_conditions'] is not None:
            extras += self.process_conditions(element['visible_conditions'], "visible_conditions")
        if 'required_conditions' in element and element['required_conditions'] is not None:
            extras += self.process_conditions(element['required_conditions'], "required_conditions")
        return extras

    def process_conditions(self, conditions, type):
        """Extracts the conditions for required/visible"""
        extra = type + '='
        cond_separator = ':'
        rule_separator = '|'
        for condition in conditions:
            extra += condition['field_key']
            extra += cond_separator
            extra += condition['operator']
            extra += cond_separator
            extra += condition['value']
            extra += rule_separator
        extra = extra.rstrip('|')
        extra += ','
        return extra

if __name__ == "__main__":
    ARGUMENTS = docopt(__doc__, version="Theta 0.1")
    PROGRAM = JtoC(ARGUMENTS)
    PROGRAM.run()
