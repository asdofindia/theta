#!/usr/bin/env python3

'''Downloads csv from google sheet'''

import os
import requests

GOOGLEDOC = os.environ.get('GOOGLEDOC', None)
if not GOOGLEDOC:
    try:
        import config
        GOOGLEDOC = config.googledoc
    except ImportError:
        raise ValueError("You need to either define GOOGLEDOC environment variable \
or put it in a config.py file with googledoc='actualkeyhere'")

def run(googledoc, sheetgid, filename):
    """Runs the function"""
    # download_url = "https://docs.google.com/spreadsheets/d/" + googledoc + "/gviz/tq?tqx=out:csv&sheet=" + sheetgid
    download_url = "https://docs.google.com/spreadsheets/d/" + googledoc + "/export?format=csv&id=" + googledoc + "&gid=" + sheetgid
    print("Downloading ", download_url)
    with open(filename, 'w') as csvfile:
        google_request = requests.get(download_url)

        # convert encoding to utf-8
        google_request.encoding = 'utf-8'

        response_text = google_request.text
        print("Got ", response_text)
        csvfile.write(response_text)

if __name__ == "__main__":
    run(GOOGLEDOC, "778246690", "housesform.csv")
    run(GOOGLEDOC, "871757358", "peopleform.csv")
